﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CsvUnknownObjectImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            var filepath = ConfigurationManager.AppSettings["CsvFilePath"];

            var runner = new UnknownObjectImporter();

            var results = runner.Run(filepath);

            Console.WriteLine(results);

            Console.ReadKey();
        }
    }
}
