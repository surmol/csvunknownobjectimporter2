﻿using CsvHelper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsvUnknownObjectImporter
{
    public class UnknownObjectImporter
    {
        public string Run(string inputFile)
        {
            if (!File.Exists(inputFile))
            {
                return "File not exist. Please check App.Config file and CsvFilePath key.";
            }
            using (var reader = new StreamReader(inputFile))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.HasHeaderRecord = true;
                csv.Configuration.MissingFieldFound = null;
                csv.Read();
                csv.ReadHeader();

                var headers = csv.Context.HeaderRecord;

                var array = new JArray() as dynamic;

                while (csv.Read())
                {
                    dynamic fullJsonObj = new JObject();

                    for (var x = 0; x < headers.Length; x++)
                    {
                        var headerName = headers[x];
                        var field = csv.GetField(x);


                        if (headers[x].Contains("_"))
                        {
                            var nestedNames = headerName.Split('_');
                            var obj = new JObject();


                            obj.Add(nestedNames[1], field);
                            if (fullJsonObj[nestedNames[0]] == null)
                            {
                                fullJsonObj.Add(nestedNames[0], obj);
                            }
                            else
                            {
                                var objNext = fullJsonObj[nestedNames[0]];
                                objNext[nestedNames[1]] = field;
                                fullJsonObj[nestedNames[0]] = objNext;
                            }
                        }
                        else
                        {
                            fullJsonObj.Add(headers[x], field);
                        }

                    }

                    array.Add(fullJsonObj);

                }


                return array.ToString();

            }
        }

    }

}
